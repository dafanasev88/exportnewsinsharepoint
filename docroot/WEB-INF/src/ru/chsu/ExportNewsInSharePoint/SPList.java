package ru.chsu.ExportNewsInSharePoint;

import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.net.URI;
import java.net.URL;
import java.net.URLDecoder;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import javax.xml.namespace.QName;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.ws.BindingProvider;

import org.apache.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.NodeList;
import org.w3c.dom.Text;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import com.microsoft.schemas.sharepoint.soap.GetListItems;
import com.microsoft.schemas.sharepoint.soap.GetListItemsResponse;
import com.microsoft.schemas.sharepoint.soap.Lists;
import com.microsoft.schemas.sharepoint.soap.ListsSoap;
import com.microsoft.schemas.sharepoint.soap.UpdateListItems;
import com.microsoft.schemas.sharepoint.soap.UpdateListItems.Updates;
import com.sun.org.apache.xerces.internal.dom.ElementNSImpl;

public class SPList {
	
	private ListsSoap port;
	private String listName;
	private enum ModeList {New, Update, Delete};
	private static Logger log = Logger.getLogger(SPList.class);
	
	public SPList(String listName) {
		this.listName = listName;
	}
	
	public void setPortSPList(Properties params) throws Exception {
			URL   wsdlURL 	= new URL(params.getProperty("news.wsdl"));
			QName query		= new QName("http://schemas.microsoft.com/sharepoint/soap/", "Lists");
			Lists service 	= new Lists(wsdlURL, query);
			port 			= service.getListsSoap();
			BindingProvider bp = (BindingProvider) port;
			bp.getRequestContext().put(BindingProvider.USERNAME_PROPERTY, params.getProperty("username"));
	        bp.getRequestContext().put(BindingProvider.PASSWORD_PROPERTY, params.getProperty("password"));
	        
	        URL convertedurl = convertURL(params.getProperty("news.endpoint"));
            bp.getRequestContext().put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, convertedurl.toString());
            log.debug("����������� � SharePoint ���������");
	}
	
	private URL convertURL(String string){
	      try {
	          String decodedURL = URLDecoder.decode(string, "UTF-8");
	          URL url = new URL(decodedURL);
	          URI uri = new URI(url.getProtocol(), url.getUserInfo(), url.getHost(), url.getPort(), url.getPath(), url.getQuery(), url.getRef());
	          return uri.toURL();
	      } catch (Exception ex) {
	          ex.printStackTrace();
	          return null;
	      }
	}
	
	
	private void addItemXMLRequest(HashMap<String, String> fields, Document doc, ModeList mode) {	         
       
		Element rootNode = doc.getDocumentElement();
		Element methodNode = doc.createElement("Method");
		methodNode.setAttribute("ID", "1");
		methodNode.setAttribute("Cmd", mode.name());
		
		for (Map.Entry<String, String> aField : fields.entrySet()) {
        	Element createdElement	= doc.createElement("Field");
            createdElement.setAttribute("Name", aField.getKey()); 
            
            Text attributeValue 	= doc.createTextNode(aField.getValue());
            createdElement.appendChild(attributeValue);
            methodNode.appendChild(createdElement);
        }
		rootNode.appendChild(methodNode);
	}
	
	private Document createXMLRequest(String xml) throws SAXException, IOException, ParserConfigurationException {
		DocumentBuilderFactory factory 	= DocumentBuilderFactory.newInstance();
        factory.setNamespaceAware(true);             
        return factory.newDocumentBuilder().parse(new InputSource(new StringReader(xml)));
	}
	
	public String xmlToString(Document docToString) {
	    String returnString = "\n-------------- XML START --------------\n";
	    try {
	        
	        TransformerFactory transfac = TransformerFactory.newInstance();
	        Transformer trans;
	        trans = transfac.newTransformer();
	        trans.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
	        trans.setOutputProperty(OutputKeys.INDENT, "yes");
	        StringWriter sw = new StringWriter();
	        StreamResult streamResult = new StreamResult(sw);
	        
	        DOMSource source = new DOMSource(docToString);
	        trans.transform(source, streamResult);
	        String xmlString = sw.toString();
	        //print the XML
	        returnString = returnString + xmlString;
	    } catch (TransformerException ex) {
	        //Logger.getLogger(Manager.class.getName()).log(Level.SEVERE, null, ex);
	    }
	    returnString = returnString + "-------------- XML END --------------";
	    return returnString;
	}
	
	public void editList(HashMap<String, String> paramsItem) throws Exception {
		
		String idSharePointItem = searchItemList(paramsItem.get("IdLiferay"));
		if (idSharePointItem != null) {
			paramsItem.put("ID", idSharePointItem);
			updateListItem(paramsItem);
		} else {
			insertItemList(paramsItem);
		}
	}
	
	private void insertItemList(HashMap<String, String> paramsItem) throws Exception {
			
		log.debug("���������� �������");
		Document doc 	= createXMLRequest("<Batch OnError='Continue' ListVersion='1'></Batch>");
		addItemXMLRequest(paramsItem, doc, ModeList.New);
		log.debug("������ ���������� �������� " + xmlToString(doc));
		Updates updates = new UpdateListItems.Updates();
		Object docObj 	= (Object) doc.getDocumentElement();
		
		updates.getContent().add(0, docObj);		
		port.updateListItems(listName, updates);
		log.debug("������� ���������");		
	}
	
	private String searchItemList(String idLiferay) throws SAXException, IOException, ParserConfigurationException {
		
		Document doc = createXMLRequest("<Query><Where><Eq><FieldRef Name='IdLiferay' /><Value Type='Number'>"+idLiferay+"</Value></Eq></Where></Query>");
		GetListItems.Query query = new GetListItems.Query();
        query.getContent().add(doc.getDocumentElement());
        
        GetListItemsResponse.GetListItemsResult result = port.getListItems(listName, null, query, null, "1", null, "");          
        Object listResult = result.getContent().get(0);
        if ((listResult != null) && (listResult instanceof ElementNSImpl)) {
        	
        	ElementNSImpl node 	= (ElementNSImpl) listResult;
            NodeList list 		= node.getElementsByTagName("z:row");
            if (list.getLength() > 0){
            	NamedNodeMap attributes = list.item(0).getAttributes();
            	return attributes.getNamedItem("ows_ID").getNodeValue();
            
            }                 
        }
        
        return null;        
	}
	
	private void updateListItem(HashMap<String, String> paramsItem) throws SAXException, IOException, ParserConfigurationException {
					
    	Document doc 	= createXMLRequest("<Batch OnError='Continue' ListVersion='1'></Batch>");			
        addItemXMLRequest(paramsItem, doc, ModeList.Update);
        log.debug("������ �� ����������: " + xmlToString(doc));
        Updates updates = new UpdateListItems.Updates();
		Object docObj 	= (Object) doc.getDocumentElement();
		updates.getContent().add(0, docObj);
		port.updateListItems(listName, updates);
		log.debug("������� ���������");
       
	}
	
}
