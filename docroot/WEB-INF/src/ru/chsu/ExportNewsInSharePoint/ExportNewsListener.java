package ru.chsu.ExportNewsInSharePoint;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.Writer;
import java.net.Authenticator;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Properties;

import org.apache.log4j.Logger;

import com.liferay.portal.ModelListenerException;
import com.liferay.portal.model.BaseModelListener;
import com.liferay.portlet.asset.model.AssetEntry;
import com.liferay.portlet.asset.service.AssetEntryLocalServiceUtil;
import com.liferay.portlet.asset.service.persistence.AssetEntryQuery;
import com.liferay.portlet.journal.model.JournalArticle;

public class ExportNewsListener extends BaseModelListener<JournalArticle> {
	
	private static Properties properties = new Properties();
	private static Logger log = Logger.getLogger(ExportNewsListener.class);
	
	@Override
    public void onAfterCreate(JournalArticle article) throws ModelListenerException {
        
		try {
			
			properties.load(getClass().getResourceAsStream("/SPPortal.properties"));
			byte numCat = isNews(article);
        	log.info("num = " + numCat);
        	
			if (numCat > 0){
							
				
				Authenticator.setDefault(new NtlmAuthenticator(properties.getProperty("username"), properties.getProperty("password")));
				
				SPList list = new SPList(properties.getProperty("news.listName").toString());
				list.setPortSPList(properties);
				
				HashMap<String, String> item = new HashMap<String, String>();
				item.put("Title", article.getTitle());
				
				String content = article.getContent().replaceAll(".+\\[|\\n|<h2.*?>.*?<\\/h2>|<script.*?><\\/script>|<div class=\"yashare-auto-init\".*?>.*?<\\/div>|]].+", "");
				content = content.replaceAll("src=\"", "src=\"http://www.chsu.ru");
				item.put("Body", content);
				
				item.put("IdLiferay", article.getArticleId());
				SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");	   
		        item.put("PublishedDate",  sdf.format(new Date()));	 
		       
		        if (numCat == 1)
					item.put("PostCategory", properties.getProperty("news.category"));
				else
					item.put("PostCategory", properties.getProperty("other.category"));
				
		        list.editList(item);
			}
	        
		} catch (IOException e) {
			log.error("Ошибка работы со статьей - " + getStackTrace(e));
		} catch (Exception e) {
			log.error("Ошибка работы со статьей - " + getStackTrace(e));		
		}
		
        super.onAfterCreate(article);
    }

    @Override
    public void onAfterUpdate(JournalArticle article) throws ModelListenerException {

        try {   
        	properties.load(getClass().getResourceAsStream("/SPPortal.properties"));
        	
        	byte numCat = isNews(article);
        	log.info("num = " + numCat);
        	if (numCat > 0){    	
				
				Authenticator.setDefault(new NtlmAuthenticator(properties.getProperty("username"), properties.getProperty("password")));
				
				SPList list = new SPList(properties.getProperty("news.listName").toString());
				list.setPortSPList(properties);
				
				HashMap<String, String> item = new HashMap<String, String>();
				item.put("Title", article.getTitle());
				item.put("Body",  article.getContent().replaceAll(".+\\[|\\n|]].+", ""));
				item.put("IdLiferay", article.getArticleId());
				if (numCat == 1)
					item.put("PostCategory", properties.getProperty("news.category"));
				else
					item.put("PostCategory", properties.getProperty("other.category"));
			
		        list.editList(item);
        	}
	        
		} catch (IOException e) {
			log.error("Ошибка работы со статьей - " + getStackTrace(e));			
		} catch (Exception e) {
			log.error("Ошибка работы со статьей - " + getStackTrace(e));
		}
        
        super.onAfterUpdate(article);
    }
    
    private byte isNews(JournalArticle article)  {

    	try {	    	    	
	    	AssetEntryQuery assetEntryQuery = new AssetEntryQuery();
	    	assetEntryQuery.setClassName(JournalArticle.class.getName());
	    	assetEntryQuery.setOrderByCol1(assetEntryQuery.ORDER_BY_COLUMNS[2]);
	    	assetEntryQuery.setOrderByType1("DESC");
	    	assetEntryQuery.setEnd(10);
	    	
	    	long[] Ids = new long[1];
	    	Ids[0] = Long.parseLong(properties.getProperty("tag.announcements"));
	    	log.info("id cat = " + Ids[0] + " " + properties.getProperty("tag.announcements"));
	    	assetEntryQuery.setAnyTagIds(Ids);
	    
	    	List<AssetEntry> assetEntryList = AssetEntryLocalServiceUtil.getEntries(assetEntryQuery);	    	
	    	for (AssetEntry ae : assetEntryList) {
	    		log.info("key " + ae.getPrimaryKey() + " - " + ae.getEntryId() + " - " + ae.getTitle() + " - " + " / " + article.getPrimaryKey());
	    		if (ae.getTitle().equals(article.getTitle())) {
	    			return 1;
	    		
	    		}
	    	}
	    	
	    	assetEntryQuery = new AssetEntryQuery();
	    	assetEntryQuery.setClassName(JournalArticle.class.getName());
	    	assetEntryQuery.setOrderByCol1(assetEntryQuery.ORDER_BY_COLUMNS[2]);
	    	assetEntryQuery.setOrderByType1("DESC");
	    	assetEntryQuery.setEnd(10);
	    	
	    	Ids = new long[1];	    	
	    	Ids[0] = Long.parseLong(properties.getProperty("category.news"));	    	
	    	assetEntryQuery.setAnyCategoryIds(Ids);
	    	assetEntryList = AssetEntryLocalServiceUtil.getEntries(assetEntryQuery);	    	
	    	for (AssetEntry ae : assetEntryList) {
	    		if (ae.getTitle().equals(article.getTitle())) {
	    			return 2;
	    			
	    		}
	    	}
	    	log.info(article.getTitle() + " not found " + article.getArticleId() + " - " + article.getId() + " - " + article.getPrimaryKey());
    	} catch (Exception e) {
			log.error("�� ������ ��� - " + e);
		}
    	
    	return 0;
    }
    
    public String getStackTrace(Throwable aThrowable) {
        Writer result = new StringWriter();
        PrintWriter printWriter = new PrintWriter(result);
        aThrowable.printStackTrace(printWriter);
        return result.toString();
    }

	//private static Log _log =LogFactoryUtil.getLog(ExportNewsListener.class);	
}

